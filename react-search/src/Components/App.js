import React, {Component} from 'react';
import ReactDOM from 'react-dom';

class App extends Component {	

	constructor(props) {
		super(props);
		this.state = {
			error: null,
			response: [],
			url: ''
		};
	}

	handleChange = (e) => {
		this.setState({
			url: this.props.url + e.target.value
		})
	}

	handleClick = () => {
		fetch(this.state.url)
		.then((res) => { 
			return res.json()
		})
		.then((data) => {
			
			const listItems = this.makeStringArray(data).map((element, i) => 
				<li key={i}>{element}</li>
			)

			this.setState({
				response: listItems
			});

		},
		(error) => {
			this.setState({
				error: error
			});
		})
	}

	makeStringArray = (data) => {
		var array = []

		for (var i in data) {
			array.push(i + ": " + data[i])
		}

		return array
	}
   
	render() {
		return (
			<section>
				<input type="text" id="username-input" onChange = {this.handleChange}/>
				<button id="search" onClick = {this.handleClick}>Search</button>
				<ul id="response" style={{'listStyle': 'none'}}>
					{this.state.error ? this.state.error.message : this.state.response}
				</ul>
			</section>
		)
	}	
}

export default App