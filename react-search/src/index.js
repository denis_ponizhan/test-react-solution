import React from 'react';
import ReactDOM from 'react-dom';
import App from './Components/App'

ReactDOM.render(
	<App url = "https://api.github.com/users/" />, 
	document.getElementById('root')
);
